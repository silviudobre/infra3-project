const express = require('express')
const cors = require('cors')
const app = express()
app.use(cors())
const port = 3000

app.use(express.static('static'))

app.get(`/`, (req, res) => {
  // res.sendFile('static/html/index.html', {root: __dirname})
  res.redirect('https://storage.cloud.google.com/infra3-project/static/html/index.html')
})

app.get(`/api/users`, (req, res) => {
    const users = [
        {
          firstName: 'Nicolas',
          lastName: 'Lorenzo',
          nationality: 'Venezolano papi'
        },
        {
          firstName: 'Silviu',
          lastName: 'Dobre',
          nationality: 'Romanian blyat'
        },
        {
          firstName: 'Say',
          lastName: 'Abdulsamed',
          nationality: 'Turkish bby'
        }
      ];
    
      res.setHeader('Content-Type', 'application/json');
      res.json(users);
  });

  app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });